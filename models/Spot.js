const mongoose = require('mongoose');

const SpotSchema = mongoose.Schema({
    name: { type: String },
    description: { type: String },
    hours: [{
        day: { type: String },
        start_time:  { type: String },
        end_time:  { type: String },
        type: { type: String },
    }],
    type: { type: String },
    menu: [{
        name: { type: String },
        description: { type: String },
        price: { 
            start : { type: Number } ,
            end : { type: Number }
        }
    }],
    picture : { type: String }
});

module.exports = mongoose.model('Spots', SpotSchema);