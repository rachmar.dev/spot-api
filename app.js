require('dotenv/config');
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');
const cors = require('cors');
const app = express();

// Middlwares
app.use(cors());
app.use(bodyParser.json());
app.use("/uploads", express.static(path.join(__dirname, 'uploads')));

// Connection to Routes
const spotRoutes = require('./routes/spots');

app.use('/spots',spotRoutes);

app.post('/upload', async (req,res) => {

    const file = req.body.base64;
    const newImageName = Math.random().toString(36).substring(7);
    const imageType = file.split(';')[0].split('/')[1];

    const base64Data = file.replace(/^data:([A-Za-z-+/]+);base64,/, '');
    fs.writeFile("uploads/"+newImageName+"."+imageType, base64Data, "base64", function (err) {
        console.log(err); 
    });

    res.send({
        link: "/uploads/"+newImageName+"."+imageType,
        success : true,
        msg: "File uploaded"
    });
    
});

app.get('/',(req,res) => {
    res.send('Index');
});

// Connection to DB
mongoose.connect(process.env.DBCONNECTION, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
    })
    .then(() => 
        console.log('DB Connected!')
    )
    .catch(err => { 
        console.log(err);
    });

// Listener
app.listen(3000);