const express = require('express');
const router = express.Router();
const Spot = require('../models/Spot');


// Index
router.get('/', async (req,res) => {

    try {
        const spots = await Spot.find();
        res.status(200).json(spots);
    } catch (error) {
        res.status(500).json({msg:error});
    }

});

// Store
router.post('/', async (req,res) => {

    const spot = new Spot({
        name: req.body.name,
        description: req.body.description,
        hours: req.body.hours,
        type: req.body.type,
        menu: req.body.menu,
        picture: req.body.picture
    });

    try {
        const savedSpot = await spot.save();
        res.status(200).json(savedSpot);
    } catch (error) {
        res.status(500).json({msg:error});
    }

});

// Show
router.get('/:_id', async (req,res) => {

    try {
        const spots = await Spot.findById(req.params._id);
        res.status(200).json(spots);
    } catch (error) {
        res.status(500).json({msg:error});
    }

});

// Patch - /spot/{id}
router.patch('/:_id', async (req,res) => {

    try {
        const spots = await Spot.findByIdAndUpdate(req.params._id,{
            name: req.body.name,
            description: req.body.description,
            hours: req.body.hours,
            type: req.body.type,
            menu: req.body.menu,
            picture: req.body.picture
        });
        res.status(200).json(spots);
    } catch (error) {
        res.status(500).json({msg:error});
    }

});
// Delete - /spot/{id}
router.delete('/:_id', async (req,res) => {

    try {
        const spots = await Spot.findByIdAndRemove(req.params._id);
        res.status(200).json(spots);
    } catch (error) {
        res.status(500).json({msg:error});
    }

});

module.exports = router;